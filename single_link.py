import json,os
import csv
import pandas as pd
from extraction_code import run,filter_header,filter_data
import time

if __name__ == "__main__":
	start = time.time()
	folder = "talkwalker"
	conf_folder = "./config/"

	if folder in os.listdir("./outputs/"):
	    folder = "./outputs/"+folder+"/"
	else:        
	    folder = "./outputs/"+folder+"/"
	    os.mkdir(folder)

	link = "https://www.talkwalker.com/pt/estudos-de-caso/orange-social-listening-case-study"
	final_list = {}
	link, output = run(link)
	final_list[link] = output
	with open(folder+"unfiltered_result.json","w") as f:
		json.dump(final_list,f,indent=4)

	# relevant_output,non_relevant_output = filter_header(final_list,conf_folder+"blacklist_keywords_and_datetime_patterns.json")
	# # print(json.dumps(relevant_output,indent=2))
	# with open(folder+"relevant_result.json","w") as f:
	# 	json.dump(relevant_output,f,indent=4)
	# with open(folder+"non_relevant_result.json","w") as f:
	# 	json.dump(non_relevant_output,f,indent=4)

	# Classifier inference
	model_config = {
		"header_para_classifier":"./models/combined_classifier",
		"label_id":{
		    0:"irrelevant",
		    1:"relevant"
		}
	}
	final_output = filter_data(final_list,model_config)
	# print(json.dumps(final_output,indent=2))
	with open(folder+"master.json","w") as f:
		json.dump(final_output,f,indent=4)

	final_dict = {}
	for url,values in final_output.items():
	    for lst in values:
	        final_dict.setdefault("url",list()).append(url)
	        final_dict.setdefault("header",list()).append(lst['header'])
	        final_dict.setdefault("header_relevance",list()).append(lst['header_relevance'])
	        final_dict.setdefault("paragraph",list()).append(lst["paragraph"])
	        final_dict.setdefault("paragraph_relevance",list()).append(lst['paragraph_relevance'])        

	df = pd.DataFrame(final_dict)
	df.to_csv(folder+"master.csv",index = False)
	print("total time taken: ",time.time() - start)
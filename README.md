# case_study_data_extraction

Extract case studies data from valid case study urls

## Failing cases:
- https://www.iea.org/articles/case-study-artificial-intelligence-for-building-energy-management-systems

## Spacy model download:
- __python -m spacy download en_core_web_sm__

# Model download link:
- Install __pytorch__ and __transformers__.
- Download the __model__ folder from the link below and place it in the directory.
- __https://drive.google.com/drive/folders/1BLsEnPUonCecZkwyrtlGYiRinriz_twb?usp=sharing__

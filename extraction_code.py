import re,json
import spacy
import requests
from bs4 import BeautifulSoup
import time
from random import randint
from transformers import DistilBertForSequenceClassification,DistilBertTokenizer
import numpy as np
import torch
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
import string
from utils import remove_tags,split_para

"""
    DATA EXTRACTION LOGIC
"""
def get_output(soup):
    data = []
    header_tag_list = ['h1', 'h2', 'h3', 'h4', 'h5','h6','strong','b']
    next_header_text_div = ""  
    next_header_text_p = ""
    child_flag_p = False
    child_flag_p_lower = False
    child_flag_li = False
    child_flag_li_lower = False
    child_flag_li_skip = []
    for header in soup.find_all(header_tag_list):
        print("-"*50)
        raw_txt = []        
        empty_header = False
        heading_text = header.get_text().strip()
        if not heading_text:    # empty header
            empty_header = True
        if re.match(r'~(^\d+)|(^[a-z]+)|^([!-,.?@#$%^&*_~]{1})',heading_text):
            print("skipped header: ",heading_text)
            continue

        # strong/b tag in p tag issue
        if child_flag_p:
            raw_txt.append(next_header_text_p)
            child_flag_p = False
        if child_flag_li_skip:
            child_flag_li_skip.pop()
            continue

        print("Accepted Header: ",heading_text)
        # find next elements
        next_tag_curr = header.findNext(re.compile('p|h\d{1,6}|li|strong|b'))
        flag = True
        while flag:
            if next_tag_curr: 
                if next_tag_curr.name in header_tag_list:  
                    break
                
                # Para tag                 
                if next_tag_curr.name == "p":
                    if next_tag_curr.findChildren(header_tag_list):
                        for next_child in next_tag_curr.findChildren(header_tag_list):
                            # if header not punctuation mark or start with small letter or start with digit
                            if not re.match(r'~(^\d+)|(^[a-z]+)|([!-,.?@#$%^&*_~]{1})',next_child.get_text()):
                                next_header_text_p = next_tag_curr.get_text().lstrip(next_child.get_text())
                                print("next_header_text_p -- ",next_header_text_p)
                                if next_header_text_p:
                                    child_flag_p = True
                                    break
                            else:
                                child_flag_p_lower = True
                                raw_txt.append(next_tag_curr.get_text().strip())
                                break
                    else:
                        raw_txt.append(next_tag_curr.get_text().strip())
 
                # list tags
                if next_tag_curr.name == "li": 
                    if next_tag_curr.findChildren(['div','a','span']):
                        print("....")
                    if next_tag_curr.findChildren(header_tag_list):
                            child_flag_li = True
                            child_flag_li_skip.append(True)
                            raw_txt.append("\n{}".format(str(next_tag_curr.get_text()).strip()))
                            print(" - bold li: \n{}".format(str(next_tag_curr.get_text()).strip()))
                    else:
                        print("called li nov")
                        raw_txt.append("\n{}".format(str(next_tag_curr.get_text()).strip()))
                        
                # Find next element 
                if not child_flag_p_lower and not child_flag_li:
                    next_tag_curr = next_tag_curr.findNext(re.compile('p|h\d{1,6}|li|strong|b'))
                else:
                    next_tag_curr = next_tag_curr.findNext(re.compile('p|li'))
                    child_flag_p_lower = False
                    child_flag_li = False
            else:
                flag = False

        # modifying the raw txt(para) data to remove duplicates
        if empty_header and not raw_txt:
            continue
        else:
            raw_txt_dict = {}
            for idx,txt in enumerate(raw_txt):
                raw_txt_dict[idx] = txt

            modif_raw_txt_dict = raw_txt_dict.copy()
            tmp_dict = {}
            modif_raw_txt = []
            max_txt_len = 0
            max_len_txt_idx = 0 
            for x,y in raw_txt_dict.items():
                if max_txt_len < len(y):
                    max_txt_len = len(y)
                    max_len_txt_idx = x
            idx = 0
            for txt in raw_txt_dict.values():
                if idx == max_len_txt_idx:
                    tmp_dict[idx] = txt
                    idx += 1
                    continue
                if txt in raw_txt_dict[max_len_txt_idx]:
                    del modif_raw_txt_dict[idx]
                    idx += 1
                else:
                    tmp_dict[idx] = txt
                    idx += 1
            # print(tmp_dict)
            for x in tmp_dict.values():
                modif_raw_txt.append(x)

            para_text = " ".join([txt for txt in modif_raw_txt if txt is not None]).strip()
            para_text = para_text.encode("ascii", "ignore")
            para_text = para_text.decode()

            heading = header.getText().strip()
            heading = heading.encode("ascii", "ignore")
            heading = heading.decode()
            if len(data) < 1:
                data.append((heading,para_text))
                continue
            if heading == data[-1][0] and para_text == data[-1][0]:
                del data[-1]
                data.append((heading,para_text))
            if heading == data[-1][0] and data[-1][0]:
                del data[-1]
                data.append((heading,para_text))
            else:
                data.append((heading,para_text))   
    return data


"""
    FILTER HEADERS METHOD
"""
# "blacklist_keywords_and_datetime_patterns.json"
def filter_header(output_json,config_json):
    data = json.load(open(config_json))
    blacklist = data["phrases"]
    date_time_patterns = data["datetime_patterns"]
    url_patterns = [r'{}'.format(x) for x in data["url_patterns"]]
    page_error_list = data["page_error"]
    nlp = spacy.load("en_core_web_sm")
    non_relevant_head_para_pair = dict()
    relevant = dict()
    for url,values in output_json.items():
        tmp_relevant = []
        tmp_non_relevant = []
        if type(values) == list:
            if values:
                # Checking if the header has connection timed out/page error key pharases then skip it
                # headers_list = [head for head in values.keys()]
                headers_list = [lst[0] for lst in values]
                page_not_exist = False
                for x in headers_list:
                    if x in page_error_list:
                        page_not_exist = True
                if not page_not_exist:
                    for lst in values:
                        head,para = lst
                        # if head:
                        # blacklist keywords phrases
                        blacklist_flag = False
                        for blk in blacklist:
                            # if head.casefold().startswith(blk.lower()):
                            if blk.lower() in head.lower():
                                blacklist_flag = True
                                # print("Blacklist key: ",head)
                                break

                        # date rime patterns
                        date_flag = True
                        for date_pattr in date_time_patterns:
                            if re.search(date_pattr,head):
                                date_flag = False
                                break

                        # Person Names as keys removal
                        name_flag = False
                        if 2 <= len(head.split()) <=3:
                            doc = nlp(head)
                            for token in doc.ents:
                                if token.label_ == "PERSON":
                                    # print(token.label_)
                                    name_flag = True
                                    break

                        # Country/Place as keys removal
                        place_flag = False
                        doc = nlp(head)
                        for token in doc.ents:
                            if token.label_ == "GPE":
                                # print(token.label_)
                                place_flag = True
                                break

                        # url pattern
                        url_pattern = False
                        for url_patr in url_patterns:
                            if re.search(url_patr,head):
                                url_pattern = True
                                break

                        top_num_headers = False     #Top 10 etc..
                        if re.search("top \d+",head.lower()):
                            top_num_headers = True
                            break
                        
                        # number,decimal,percentile
                        num_flag = False
                        txt = re.sub("[.%,!]+","",head.lower())
                        txt = txt.strip() 
                        if txt.isnumeric():
                            num_flag = True

                        para = clean_para(para,date_time_patterns,url_patterns)

                        # print(head)
                        if head.strip().endswith("?"):
                            print("question!!!")
                            tmp_non_relevant.append((head,para))
                        elif blacklist_flag:
                            print("blacklist!!!")
                            tmp_non_relevant.append((head,para))
                        elif not date_flag:
                            print("date/time!!!")
                            tmp_non_relevant.append((head,para))
                        elif name_flag:
                            print("Name found!!!")
                            tmp_non_relevant.append((head,para))
                        elif url_pattern:
                            print("url_pattern found!!!")
                            tmp_non_relevant.append((head,para))
                        elif place_flag:
                            print("place found!!!")
                            tmp_non_relevant.append((head,para))
                        elif top_num_headers:
                            print("header containing 'top num' link 'top 10 tools' phrase!!!")
                            tmp_non_relevant.append((head,para))
                        elif num_flag:
                            print("header containing number,decimal percentage only!!!")
                            tmp_non_relevant.append((head,para))
                        else:                
                            print("called relevant")
                            tmp_relevant.append((head,para))
                        # else:
                        #     tmp_non_relevant.append((head,para))
                    relevant[url] = tmp_relevant
                    non_relevant_head_para_pair[url] = tmp_non_relevant
    return relevant,non_relevant_head_para_pair

"""
    Method to clean the paragraph.Called inside filter_header() method.
"""
def clean_para(para,date_time_patterns,url_patterns):
    para = para.replace("-->"," ").replace("None"," ")
    para = re.sub(' +', ' ', para)
    para = re.sub('\n', ' ', para)
    para = re.sub('\t', ' ', para)
    for date_pattr in date_time_patterns:
        para = re.sub(date_pattr,'',para)
    for url_patr in url_patterns:
        para = re.sub(url_patr,'',para)
    return para

"""
    Method to check whether both headers and paras are relevant or not
    -- Parameter:
        - output_json: filtered data
    -- Returns:
        a tuple of relevant_json/filtered output and non_relevant_json.
"""
def filter_data(output_json,model_config):    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # Load header - para combined classifier model
    combined_model = DistilBertForSequenceClassification.from_pretrained(model_config["header_para_classifier"])
    combined_tokenizer = DistilBertTokenizer.from_pretrained(model_config["header_para_classifier"])
    combined_model = combined_model.to(device)

    non_relevant_head_para_pair = dict()
    relevant = dict()
    for url,values in output_json.items():
        tmp_relevant = []
        # tmp_non_relevant = []
        if values:
            for idx,lst in enumerate(values):
                head,para = lst   
                header_relevance = "False"
                para_relevance = "False"
                if head:
                    head = "[H{}] {}".format(idx,head)
                    head_copy = head
                    head_copy = re.sub(r'[\(\[].*?[\)\]]\s+','',head_copy)
                    
                    # Heading inference     
                    header_predict_input_pt = combined_tokenizer.encode(head_copy,truncation=True,padding=True,return_tensors="pt")
                    header_output_pt = combined_model(header_predict_input_pt)[0]
                    header_predictions_value_pt = torch.argmax(header_output_pt[0], dim=-1).item()                    
                    if header_predictions_value_pt == 1:
                        header_relevance = "True"

                if para:
                    # split para into 4 segments 
                    para_list = split_para(para)
                    for ix,txt in enumerate(para_list):
                        # Para inference 
                        para_predict_input_pt = combined_tokenizer.encode(txt,truncation=True,padding=True,return_tensors="pt")
                        para_output_pt = combined_model(para_predict_input_pt)[0]
                        para_predictions_value_pt = torch.argmax(para_output_pt[0], dim=-1).item()                        
                        if para_predictions_value_pt == 1:
                            para_relevance = "True"
                        tmp_relevant.append({
                            "header":head,
                            "header_relevance":header_relevance,
                            "paragraph":"[P{}_section{}] {}".format(idx,ix,txt),
                            "paragraph_relevance":para_relevance
                        })
                else:
                    tmp_relevant.append({
                        "header":head,
                        "header_relevance":header_relevance,
                        "paragraph":para,
                        "paragraph_relevance":para_relevance
                    })
                        
            relevant[url] = tmp_relevant
            # non_relevant_head_para_pair[url] = tmp_non_relevant
    return relevant

"""
    MAKE REQUESTS
"""
def run(link):
    req_headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
    }
    try:
        page = requests.get(link,headers=req_headers)        
        print(link)
    except Exception as e:
        print(e)
        return link,""
    else:
        time.sleep(randint(5,15))   
        soup = BeautifulSoup(page.text, 'html.parser')
        data = get_output(remove_tags(soup))
        return link,data
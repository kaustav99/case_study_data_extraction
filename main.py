import json,os
import csv
import pandas as pd
from extraction_code import run,filter_header,filter_data
import time


if __name__ == "__main__":    
    start_t = time.time()
    folder = "unique_links_feb15_v1"
    conf_folder = "./config/"

    if folder in os.listdir("./outputs/"):
        folder = "./outputs/"+folder+"/"
    else:        
        folder = "./outputs/"+folder+"/"
        os.mkdir(folder)


    """
        Create case studies set
    """
    case_studies = json.load(open(conf_folder+"unique_links_feb15_v1.json"))
    print(len(case_studies))


    """
    Generate raw csv and json
    """
    final_list = {}
    for index,case_study_links in enumerate(case_studies):
        print("{} List....".format(index))
        for link_id,link in enumerate(list(set(case_study_links))):
            try:
                start = time.time()
                print(link_id)
                link, output = run(link)
                final_list[link] = output
                with open(folder+"unfiltered_result.csv", "a", newline='') as fp:
                    wr = csv.writer(fp, dialect='excel')
                    wr.writerow([link,output])
                end = time.time()
                print("Total time taken: {} seconds.".format(end-start))
            except Exception as e:
                print(e)

    with open(folder+"unfiltered_result.json","w") as f:
        json.dump(final_list,f,indent=4)
    
    """
        Generate unfiltered csv files for header ,para pair
    """
    data = json.load(open(folder+"unfiltered_result.json"))
    head_lst = []
    para_lst = []
    head_url = []
    para_url = []
    head_lst_full = []
    para_lst_full = []
    url_lst = []
    for url,values in data.items():
        if type(values) == list:
            for lst in values:
                key = lst[0]
                val = lst[1]
                if key:
                    head_url.append(url)
                    head_lst.append(str(key))
                if val:
                    para_url.append(url)
                    para_lst.append(str(val))
                head_lst_full.append(str(key))
                para_lst_full.append(str(val))
                url_lst.append(url)

    df = pd.DataFrame({"URL":head_url,"Header":head_lst})
    df2 = pd.DataFrame({"URL":para_url,"Paragraphs":para_lst})
    df3 = pd.DataFrame({"URL":url_lst,"Header":head_lst_full,"Paragraphs":para_lst_full})
    df.to_csv(folder+"heading_dataset_unfiltered.csv",index = False)
    df2.to_csv(folder+"paragraphs_dataset_unfiltered.csv",index = False)
    df3.to_csv(folder+"combined_dataset_unfiltered.csv",index = False)


    """
        Result filteration and csv files and json generation
    """
    # relevant_output,non_relevant_output = filter_header(json.load(open(folder+"unfiltered_result.json")),conf_folder+"blacklist_keywords_and_datetime_patterns.json")
    # with open(folder+"relevant_results.json","w") as f:
    #     json.dump(relevant_output,f,indent=4)
    # with open(folder+"non_relevant_results.json","w") as f:
    #     json.dump(non_relevant_output,f,indent=4)
    
    # Classifier inference
    model_config = {
        "header_para_classifier":"./models/combined_classifier",
        "label_id":{
            0:"irrelevant",
            1:"relevant"
        }
    }
    final_output = filter_data(final_list,model_config)
    # print(json.dumps(final_output,indent=2))
    with open(folder+"master.json","w") as f:
        json.dump(final_output,f,indent=4)

    final_dict = {}
    for url,values in final_output.items():
        for lst in values:
            final_dict.setdefault("url",list()).append(url)
            final_dict.setdefault("header",list()).append(lst['header'])
            final_dict.setdefault("header_relevance",list()).append(lst['header_relevance'])
            final_dict.setdefault("paragraph",list()).append(lst["paragraph"])
            final_dict.setdefault("paragraph_relevance",list()).append(lst['paragraph_relevance']) 

    df = pd.DataFrame(final_dict)
    df.to_csv(folder+"master.csv",index = False)    
    print("total time taken: ",time.time() - start_t)

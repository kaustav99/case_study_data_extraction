# Import Module
from bs4 import BeautifulSoup
import requests
import re
import spacy

"""
	Noise removal
"""
def remove_tags(soup):
	regex_header = re.compile('^header|^navbar')
	regex_footer = re.compile('footer|mz|lightbox|nav|foot|shariff|bottom|enquiry|related-content|LicenseInfo')
	regex_social_media = re.compile('social|subscribe|affiliateCard|tag|notification|announcement|author|infobox|comment')
	regex_button = re.compile('button|btn')
	regex_em = re.compile('widget')
	regex_css = re.compile('css')
	regex_cookie = re.compile('cookie|cc-banner')
	regex_sidebar = re.compile('sidebar')
	regex_modal = re.compile('modal|popup')
	regex_references = re.compile('References|article-info-section')
	# soup.find_all("div", {"class" : regex})

	div_class_header = soup.findAll("div", {"class" : regex_header})
	div_class_footer = soup.findAll(["div","section"],{"class" : regex_footer})
	div_id_sidebars = soup.findAll(["div","aside"], {"id" : regex_sidebar})
	div_class_sidebars = soup.findAll(["div","aside"], {"class":regex_sidebar})
	dragdiv = soup.findAll("div", {"id" :  "dragdiv"})
	divcom  = soup.findAll("div",{'class' : 'comments'})
	div_class_em = soup.findAll("div",{"class" : regex_em})
	modals = soup.findAll(["div","section"],{"class":regex_modal})
	cookie = soup.findAll("div",{"data-nosnippet":"true"})
	div_id_social = soup.findAll("div", {"id" : regex_social_media})
	div_class_social = soup.findAll("div", {"class":regex_social_media})
	a_button_cls = soup.findAll('a',{"class":regex_button})
	a_button_id = soup.findAll('a',{"id":regex_button})
	div_sec_reference = soup.findAll(['div','section'],{"data-title":regex_references})
	div_sec_reference_id = soup.findAll(['div','section'],{"id":regex_references})
	scripts = soup.findAll(['head','nav','script', 'footer','aside','img','figure','svg','button','app-related-posts'])

	# Save links & style - append it at the end of <body> tag 
	css_obj = soup.find_all('style')
	css_reg_obj = soup.find_all('link', {'href' : regex_css})
	for item in css_obj:
	    soup.html.body.append(item)
	for link in css_reg_obj:
	    soup.html.body.append(link)

	# Remove unneccessary tags
	for divh in div_class_header:
	    divh.decompose()
	for divr in div_sec_reference:
	    divr.decompose()
	for divh in div_sec_reference_id:
	    divh.decompose()
	for divf in div_class_footer:
	    divf.decompose()
	for divside in div_id_sidebars:
	    # print(divside)
	    divside.decompose()
	for divdrag in dragdiv:
	    print(divdrag)
	    divdrag.decompose()
	for com in divcom:
	    com.decompose()    
	for match in cookie:
	    match.decompose()
	for match in modals:
	    match.decompose()
	for match in div_class_sidebars:
	    match.decompose()
	for match in div_id_social:
	    match.decompose()
	for match in div_class_social:
	    match.decompose()
	for match in a_button_cls:
	    match.decompose()
	for match in a_button_id:
	    match.decompose()
	for match in scripts:
	    match.decompose()

	# return data by retrieving the tag content
	# return ' '.join(soup.stripped_strings)
	return soup

"""
	Split para utility
"""
#load core english library
nlp = spacy.load("en_core_web_sm")

def split_para(paragraph):
	para_list = []
	doc = nlp(paragraph)
	temp_list = []
	for idx1, sent in enumerate(doc.sents):
		temp_list.append(sent.text)
	n = 4
	temp_final_chunks = [temp_list[i * n:(i + 1) * n] for i in range((len(temp_list) + n - 1) // n )]
	if temp_final_chunks:
		for chunk in temp_final_chunks:
		  final_para = ""
		  final_para = ' '.join(chunk)
		  para_list.append(final_para)
	else:
		para_list.append(paragraph)
	return para_list


if __name__ == "__main__":
	# Website URL
	# URL = 'http://ai.googleblog.com/2021/08/a-dataset-exploration-case-study-with.html'
	URL = 'https://netbasequid.com/blog/amtrak-valentines-day-social-analytics-case-study/'

	req_headers = {
		'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
	}
	try:
		page = requests.get(URL,headers=req_headers)        
		print(URL)
	except Exception as e:
		print(e)
	else:
		# time.sleep(randint(5,15))   
		soup = BeautifulSoup(page.text, 'html.parser')
		# print(remove_tags(page.content))
		modified_soup_obj = remove_tags(soup)
		if modified_soup_obj:
			final_list = {link:get_output(modified_soup_obj)}
			import json
			with open("single_output_netbasequid.json","w") as f:
				json.dump(final_list,f,indent=4)
			html_data = modified_soup_obj.prettify('utf-8')
			with open('netbasequid.html','wb') as file:
				file.write(html_data)
